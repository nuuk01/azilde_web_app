from django.contrib import admin
from .models import PostType,CommentToComment,Comment,Post,PostUpDown,PostUpDownHistory
# Register your models here.
admin.site.register(PostType)
admin.site.register(CommentToComment)
admin.site.register(Comment)
admin.site.register(Post)
admin.site.register(PostUpDown)
admin.site.register(PostUpDownHistory)
