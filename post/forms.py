from django import forms
from .models import Post, Comment

class PostUploadModelForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['postTitle','postText','image']

class CommentUploadModelForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']