from django.contrib.auth.models import User
from slugify import slugify, Slugify, UniqueSlugify
from django.urls import reverse
from django.db import models
from random import randint
from PIL import Image as im
import os
import sys
import misaka 

# # Create your models here.
class PostType(models.Model):
     postType = models.CharField(max_length=50)
     def __str__(self):
         return "Type : {}".format(self.postType)
     class Meta:
         ordering = ['postType',]

class Post(models.Model):
     postTitle = models.CharField("Пост есімі", max_length=10000)
     postText = models.TextField("Пост тексті")
     postText_html = models.TextField(editable = False)
     image = models.ImageField("Пост суреті",null=True, blank=True, upload_to='post/post_image')
     slug = models.SlugField(allow_unicode=True,unique=True, blank=True)
     created_date = models.DateTimeField(auto_now= True,)
     updated_date = models.DateTimeField(auto_now= True,)
     author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="authorOfPosts",)
     def __str__(self):
         return "Title : {}".format(self.postTitle)
     def get_absolute_url(self):
         return reverse('post_slug_detail', kwargs = {'slug' : self.slug })
     def save(self, *args, **kwargs):
         self.slug = slugify("{}{}".format(randint(1,100000), self.postTitle))
         self.postText_html = misaka.html(self.postText)
         super().save(*args,**kwargs)
         #if self.image is not None:
            #img = im.open(self.image.path)
            #if img.height != 750 or img.width != 300:
                #output_size = (300,750)
                #img.thumbnail(output_size)
                #img.save(self.image.path)

class PostUpDown(models.Model):
    upVote = models.IntegerField(default=0)
    downVote = models.IntegerField(default=0)
    postOfVote = models.OneToOneField(Post, null=True, blank=True, related_name="postUpDown",
                                      on_delete=models.CASCADE)
    def __str__(self):
        return "Post: {}, upVote: {}, downVote: {}".format(self.postOfVote.postTitle, self.upVote, self.downVote)

class PostUpDownHistory(models.Model):
    downVote = models.BooleanField(default=False)
    upVote = models.BooleanField(default=False)
    postOfVote = models.ForeignKey(Post, null=True, blank=True, related_name="postOfVote",
                                      on_delete=models.CASCADE)
    voteUser = models.ForeignKey(User, on_delete=models.CASCADE, related_name="authorOfVote")
    def __str__(self):
        return "History: Post: {}, User: {}, upVote: {}, downVote: {}".format(self.postOfVote.postTitle, self.voteUser, self.upVote, self.downVote)

class Comment(models.Model):
     text = models.TextField("Пост комментті", max_length=100)
     created_date = models.DateTimeField(auto_now= True,)
     commentAuthor = models.ForeignKey(User, on_delete=models.CASCADE, related_name="authorOfComments")
     postOfComment = models.ForeignKey(Post, null=True, blank=True, related_name="commentOfPost", on_delete=models.CASCADE)
     def __str__(self):
         return "Comment: {}".format(self.text)
     class Meta:
         ordering = ["-created_date"]

class CommentToComment(models.Model):
     text = models.CharField(max_length=100)
     created_date = models.DateTimeField(auto_now=True,)
     commentAuthor = models.ForeignKey(User, on_delete=models.CASCADE)
     commentToComment = models.ForeignKey(Comment, related_name="commentToComments", on_delete=models.CASCADE)
     def __str__(self):
         return "CommentToComment: {}".format(self.text)