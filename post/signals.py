from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Post,PostUpDown

@receiver(post_save, sender=Post)
def create_post(sender, instance, created, **kwargs):
    if created:
        PostUpDown.objects.create(postOfVote = instance, upVote=0, downVote=0)

@receiver(post_save, sender=Post)
def save_post(sender, instance, **kwargs):
    instance.postUpDown.save()