from django import template
from datetime import date,datetime,timezone
import pytz
from django.template.defaultfilters import stringfilter
register = template.Library()

@register.filter
def get_due_date_string(value):
    almaty_time = pytz.timezone('Asia/Almaty')
    now_date = datetime.now(almaty_time)
    diff_time = now_date - value
    if diff_time.days > 0:
        return "{} күн бұрын".format(diff_time.days)
    if (diff_time.seconds // 3600) > 0:
        return "{} сағат бұрын".format(diff_time.seconds // 3600)
    if (diff_time.seconds // 60) > 0:
        return "{} минут бұрын".format(diff_time.seconds // 60)
    return "{} секунд бұрын".format(diff_time.seconds)