"""azilde_web_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
urlpatterns = [
    path('', views.IndexUpVoteClass.as_view(), name='home'),
    path('home2/', views.IndexClass.as_view(), name='home2'),
    path('home3/', views.IndexNumberOfCommentsClass.as_view(), name='home3'),
    path('<slug:slug>/', views.PostSlugDetailView.as_view(), name='post_slug_detail'),
    path('/new/', views.PostCreateView.as_view(), name='post-create'),
    path('<int:pk>/update/', views.PostUpdateView.as_view(), name='post-update'),
    path('<int:pk>/delete/', views.PostDeleteView.as_view(), name='post-delete'),
    path('<slug:slug>/comment', views.add_comment, name='add_comment'),
    path('add_up_down/', views.add_up_down, name='add_up_down'),
    path('home2/add_up_down/', views.add_up_down, name='add_up_down2'),
    path('home3/add_up_down/', views.add_up_down, name='add_up_down3'),
    path('<slug:slug>/add_up_down4/', views.add_up_down4, name='add_up_down4'),
]
