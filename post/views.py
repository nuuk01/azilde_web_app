import pytz
from .models import Post,Comment,PostUpDown,PostUpDownHistory
from datetime import date,datetime,timezone, timedelta
from .forms import PostUploadModelForm,CommentUploadModelForm
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.urls import reverse_lazy 
from django.shortcuts import render,redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
# Create your views here.
class IndexClass(ListView):
    model = Post
    ordering = ['-created_date']
    context_object_name = 'posts'
    template_name = 'post/index.html'
    paginate_by = 20
    def get_queryset(self):
        posts = super().get_queryset()
        almaty_time = pytz.timezone('Asia/Almaty')
        now_date = datetime.now(almaty_time)
        now_days_date = now_date - timedelta(days=14)
        return posts.filter(created_date__gt = now_days_date).order_by('-created_date')

class IndexUpVoteClass(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'post/index.html'
    paginate_by = 20
    def get_queryset(self):
        almaty_time = pytz.timezone('Asia/Almaty')
        now_date = datetime.now(almaty_time)
        now_14_date = now_date - timedelta(days=21)
        posts = super().get_queryset().filter(created_date__gt = now_14_date)
        sql = 'SELECT t.id,t.postTitle,t.postText,t.postText_html,t.image,t.slug,t.upVote,t.downVote,t.created_date,t.updated_date,t.author_id FROM(SELECT pp.id,pp.postTitle,pp.postText,pp.postText_html,pp.image,pp.slug,ppd.upVote,ppd.downVote,pp.created_date,pp.updated_date,pp.author_id, (ppd.upVote + ppd.downVote) as tt_vote FROM post_postupdown ppd, post_post pp WHERE ppd.postOfVote_id = pp.id ) t ORDER BY t.tt_vote DESC'
        postsRAW = posts.raw(sql)
        return postsRAW

class IndexNumberOfCommentsClass(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'post/index.html'
    paginate_by = 20
    def get_queryset(self):
        almaty_time = pytz.timezone('Asia/Almaty')
        now_date = datetime.now(almaty_time)
        now_14_date = now_date - timedelta(days=14)
        posts = super().get_queryset().filter(created_date__gt = now_14_date)
        sql = 'SELECT t.id,t.postTitle,t.postText,t.postText_html,t.image,t.slug,t.upVote,t.downVote,t.created_date,t.updated_date,t.author_id,t.cnt_comment FROM (SELECT pp.id,pp.postTitle,pp.postText,pp.postText_html,pp.image,pp.slug,ppd.upVote,ppd.downVote,pp.created_date,pp.updated_date,pp.author_id,count(pc.id)as cnt_comment FROM post_comment pc, post_postupdown ppd, post_post pp WHERE pc.postOfComment_id = pp.id and ppd.postOfVote_id = pp.id GROUP BY pp.id,pp.postTitle,pp.postText,pp.postText_html,pp.image,pp.slug,ppd.upVote,ppd.downVote,pp.created_date,pp.updated_date,pp.author_id) t ORDER BY t.cnt_comment DESC'
        postsRAW = posts.raw(sql)
        return postsRAW

class SearchPostClass(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'post/index.html'
    paginate_by = 20
    def get_queryset(self):
        query = self.request.GET.get('q')
        almaty_time = pytz.timezone('Asia/Almaty')
        now_date = datetime.now(almaty_time)
        now_14_date = now_date - timedelta(days=14)
        posts = super().get_queryset().filter(created_date__gt = now_14_date, postTitle__contains=query)

class PostSlugDetailView(DetailView):
    model = Post
    template_name = 'post/post_detail.html'    

class PostCreateView(LoginRequiredMixin, CreateView):
    form_class = PostUploadModelForm
    template_name = 'post/post_new_form.html'
    def form_valid(self,form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    form_class = PostUploadModelForm
    template_name = 'post/post_form.html'
    def form_valid(self,form):
        form.instance.author = self.request.user
        return super().form_valid(form)    
    def test_func(self):
        post = get_object_or_404(Post, pk=self.kwargs['pk'])
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    form_class = PostUploadModelForm
    template_name = 'post/post_form_delete.html'
    success_url = '/'
    def test_func(self):
        post = get_object_or_404(Post, pk=self.kwargs['pk'])
        if self.request.user == post.author:
            return True
        return False

def add_comment(request,slug):
    post = get_object_or_404(Post, slug = slug)
    commentUser = get_object_or_404(User, id = request.user.id)
    if request.method == 'POST':
        textComment = request.POST['comment']
        if textComment != "":
            comment = Comment(postOfComment = post,commentAuthor = commentUser,text = textComment)
            comment.save()
    return redirect('post_slug_detail', slug = slug)

def add_up_down(request):
    print(123)
    if request.method == 'GET':
        post_id = request.GET.get('post_id')
        user_id = request.GET.get('user_id')
        upVote = request.GET.get('upVote')
        downVote = request.GET.get('downVote')
        print("post_id : {}".format(post_id))
        print("user_id : {}".format(user_id))
        print("Upvote : {}".format(type(upVote)))
        print("Downvote : {}".format(type(downVote)))
        if user_id == "None":
            return HttpResponse("undefined")
        post = get_object_or_404(Post, id = post_id)
        up_down_User = get_object_or_404(User, id = user_id)
        up_down_post = get_object_or_404(PostUpDown, postOfVote=post)
        print(up_down_post)
        if upVote == "1":
            p_up_down_history_past_all = PostUpDownHistory.objects.filter(downVote=False, upVote=True, postOfVote=post, voteUser=up_down_User)
        else:
            p_up_down_history_past_all = PostUpDownHistory.objects.filter(downVote=True, upVote=False, postOfVote=post, voteUser=up_down_User)
        if p_up_down_history_past_all:
            return HttpResponse('default')
        if upVote == "1":
            p_up_down_history = PostUpDownHistory(downVote=False, upVote=True, postOfVote=post, voteUser=up_down_User)
            up_down_post.upVote = up_down_post.upVote + 1
            p_up_down_history.save()
        else:
            p_up_down_history = PostUpDownHistory(downVote=True, upVote=False, postOfVote=post, voteUser=up_down_User)
            up_down_post.downVote = up_down_post.downVote - 1
            p_up_down_history.save()
        up_down_post.save()
        print(up_down_post)
        return HttpResponse('success')
    else:
        return HttpResponse("error")

def add_up_down4(request,slug):
    print(123)
    if request.method == 'GET':
        post_id = request.GET.get('post_id')
        user_id = request.GET.get('user_id')
        upVote = request.GET.get('upVote')
        downVote = request.GET.get('downVote')
        print("post_id : {}".format(post_id))
        print("user_id : {}".format(user_id))
        print("Upvote : {}".format(type(upVote)))
        print("Downvote : {}".format(type(downVote)))
        if user_id == "None":
            return HttpResponse("undefined")
        post = get_object_or_404(Post, id = post_id)
        up_down_User = get_object_or_404(User, id = user_id)
        up_down_post = get_object_or_404(PostUpDown, postOfVote=post)
        print(up_down_post)
        if upVote == "1":
            p_up_down_history_past_all = PostUpDownHistory.objects.filter(downVote=False, upVote=True, postOfVote=post, voteUser=up_down_User)
        else:
            p_up_down_history_past_all = PostUpDownHistory.objects.filter(downVote=True, upVote=False, postOfVote=post, voteUser=up_down_User)
        if p_up_down_history_past_all:
            return HttpResponse('default')
        if upVote == "1":
            p_up_down_history = PostUpDownHistory(downVote=False, upVote=True, postOfVote=post, voteUser=up_down_User)
            up_down_post.upVote = up_down_post.upVote + 1
            p_up_down_history.save()
        else:
            p_up_down_history = PostUpDownHistory(downVote=True, upVote=False, postOfVote=post, voteUser=up_down_User)
            up_down_post.downVote = up_down_post.downVote - 1
            p_up_down_history.save()
        up_down_post.save()
        print(up_down_post)
        return HttpResponse('success')
    else:
        return HttpResponse("error")
