$(document).ready(function () {

const url_up = "../static/icons/up-arrow.png";
const url_down = "../static/icons/down-arrow.png";
const url_up1 = "../static/icons/up-arrow-1.png";
const url_down1 = "../static/icons/down-arrow-1.png";

$('.card').each(function () {
   $(this).find('.up_img').attr('src', url_up);
   $(this).find('.down_img').attr('src', url_down);
});

$('.upVote-click').click(function () {
    console.log("upVote");
    let up_id = $(this).find('.post_id').val();
    let upVote = $(this).find('.upVote').val();
    let downVote = $(this).find('.downVote').val();
    let user_id = $(this).find('.user_id').val();
    let up_img = $(this).find(".up_img");
    let vote_number = $(this).find(".vote_number");
    console.log(vote_number);
    console.log(vote_number.val());
    console.log(vote_number.text());
    console.log(vote_number.html());
    let upVoteCall = $.ajax( {
            type: "GET",
            url:  "add_up_down",
            data: {
                post_id: up_id,
                user_id: user_id,
                upVote: upVote,
                downVote: downVote
            },
            success : function(data){
                console.log(data);
                if (data == "undefined"){
                    alert("Ұпай қосу үшін теркеліңіз.");
                }
                if (data == "success"){
                    up_img.attr('src', url_up1);
                }
            },
            error : function(data){
                console.log(data);
            }
    });
});
$('.downVote-click').click(function () {
    console.log("downVote");
    let up_id = $(this).find('.post_id').val();
    let upVote = $(this).find('.upVote').val();
    let downVote = $(this).find('.downVote').val();
    let user_id = $(this).find('.user_id').val();
    let down_img = $(this).find(".down_img");
    let downVoteCall = $.ajax( {
            type: "GET",
            url:  "add_up_down",
            data: {
                post_id: up_id,
                user_id: user_id,
                upVote: upVote,
                downVote: downVote
            },
            success : function(data){
                console.log(data);
                if (data == "undefined"){
                    alert("Ұпай қосу үшін теркеліңіз.");
                }
                if (data == "success"){
                    down_img.attr('src', url_down1);
                }
            },
            error : function(data){
                console.log(data);
            }
    });
});
$('.upVote-click1').click(function () {
    console.log("upVote");
    let up_id = $(this).find('.post_id').val();
    let upVote = $(this).find('.upVote').val();
    let downVote = $(this).find('.downVote').val();
    let user_id = $(this).find('.user_id').val();
    let up_img = $(this).find(".up_img");
    let upVoteCall = $.ajax( {
            type: "GET",
            url:  "add_up_down4",
            data: {
                post_id: up_id,
                user_id: user_id,
                upVote: upVote,
                downVote: downVote
            },
            success : function(data){
                console.log(data);
                if (data == "undefined"){
                    alert("Ұпай қосу үшін теркеліңіз.");
                }
                if (data == "success"){
                    up_img.attr('src', url_up1);
                }
            },
            error : function(data){
                console.log(data);
            }
    });
});
$('.downVote-click1').click(function () {
    console.log("downVote");
    let up_id = $(this).find('.post_id').val();
    let upVote = $(this).find('.upVote').val();
    let downVote = $(this).find('.downVote').val();
    let user_id = $(this).find('.user_id').val();
    let down_img = $(this).find(".down_img");
    let downVoteCall = $.ajax( {
            type: "GET",
            url:  "add_up_down4",
            data: {
                post_id: up_id,
                user_id: user_id,
                upVote: upVote,
                downVote: downVote
            },
            success : function(data){
                console.log(data);
                if (data == "undefined"){
                    alert("Ұпай қосу үшін теркеліңіз.");
                }
                if (data == "success"){
                    down_img.attr('src', url_down1);
                }
            },
            error : function(data){
                console.log(data);
            }
    });
});
});
