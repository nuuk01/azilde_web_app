from django import forms 
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile
class UserRegisterForm(UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': "Кілт сөздер сәйкес келмейді.",
        'username_exists': "Бұндай есім бос емес.",
    }
    username = forms.CharField(label='Есімі', max_length=32, required=True, help_text='Толтыруыңыз керек.')
    email = forms.EmailField(label="Почта", required=True, max_length=64)
    password1 = forms.CharField(label="Кілт сөз", required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(label="Кілт сөзді қайталамасы", required=True,
                                widget=forms.PasswordInput,
                                help_text="Кілт сөзді қайталап жазыңыз.")
    class Meta:
        model = User
        fields = ['username','email','password1','password2']

    def clean_username(self):
        username = self.cleaned_data.get("username")
        try:
            User._default_manager.get(username=username)
            # if the user exists, then let's raise an error message
            raise forms.ValidationError(
                self.error_messages['username_exists'],  # my error message
                code='username_exists',  # set the error message key
            )
        except User.DoesNotExist:
            return username  # if user does not exist so we can continue the registration process

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username','email']

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']

class UserLoginForm(forms.ModelForm):
    username = forms.CharField(label='Есімі', max_length=32, required=True, help_text='Толтыруыңыз қажет.')
    password1 = forms.CharField(label="Кілт сөз", required=True, widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ['username','password1']
