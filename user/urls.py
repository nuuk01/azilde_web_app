from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
urlpatterns = [
    path('register/', views.register, name='register'),   
    path('profile/', views.user_profile, name='profile'),
    path('<int:user_id>/profile/', views.profile, name='author_profile'),      
    path('login/', views.log_in, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='user/logout.html'), name='logout'),   
]