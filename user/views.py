from user.models import User,Profile
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from .forms import UserRegisterForm,UserUpdateForm,ProfileUpdateForm,UserLoginForm
from django.contrib.auth import authenticate, login, logout
from django import forms
# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(data = request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return redirect('home')
    else:
        form = UserRegisterForm()
    return render(request, 'user/signup.html',{'form' : form})

@login_required
def user_profile(request):
    return render(request, 'user/profile.html')

def log_in(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password1')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('home')
        else:
            return redirect('login')
    else:
        form = UserLoginForm()
        return render(request, 'user/login.html', {'form' : form})

@login_required
def profile(request, user_id):
    user = get_object_or_404(User, id = user_id)
    #if request.method == 'POST':
    #    u_form = UserUpdateForm(request.POST, instance = request.user)
    #    p_form = ProfileUpdateForm( request.POST,
    #                                request.FILES,
    #                                instance = request.user.profile)
    #    if u_form.is_valid() and p_form.is_valid():
    #        u_form.save()
    #        p_form.save()
    #        return redirect('profile')
    #else:
    #    print(request.user)
    #    print(request.user.profile)
        #u_form = UserUpdateForm(instance = request.user)
        #p_form = ProfileUpdateForm(instance = request.user.profile)
    return render(request,'user/profile.html',{'user': user})
